package frc.team8060.subsystems;

import com.revrobotics.*;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.team8060.constants.ArmConstants;

public class ArmControl extends SubsystemBase {

    /*
        IMPORTANT

        gear ratio of 3.2 means encoder distance of

        0 encoder distance is equivalent to arm in starting position, facing upwards
        positive values of distance moves arm down

        make sure arm is in upwards position!!!!

        currently positive values of encoder and power = up
        negative = down

     */

    private final CANSparkMax motorController = new CANSparkMax(
            ArmConstants.ARM_MOTOR_PORT, CANSparkMaxLowLevel.MotorType.kBrushed
    );

    private RelativeEncoder encoder;

    // distance values are NOT adjusted
    private double distance = 0;
    private double previousDistance = 0;

    private final double GEAR_RATIO = 3.2; // big gear is 48 and small is 15
    private final double MAX_DISTANCE = 300; // relative to encoder -> 90 * 3.2 = 288 + wiggle room
    private final double MIN_DISTANCE = -10; // estimate for now
    private final double MAX_POWER = 0.7;

    public ArmControl() {

        // try to init encoder
        try {
            // encoder = motorController.getAbsoluteEncoder(SparkMaxAbsoluteEncoder.Type.kDutyCycle);
            encoder = motorController.getEncoder(SparkMaxRelativeEncoder.Type.kQuadrature, 8192);
        } catch (Exception e) {
            e.printStackTrace();
            encoder = null;
        }
        if (encoder != null)
            encoder.setInverted(ArmConstants.ENCODER_INVERTED);
    }

    @Override
    public void periodic() {
        if (encoder != null) {
            previousDistance = distance;
            distance = encoder.getPosition();
            SmartDashboard.putNumber("arm_distance_degrees", distance/3.2);
            double vel = encoder.getVelocity();
            SmartDashboard.putNumber("rpm", vel);
            SmartDashboard.putNumber("arm_velocity_degrees_per_sec", (vel * 360) / 60);
        }

        // for now this is temporary to try and keep shit from breaking
        /*
        if (distance > MAX_DISTANCE_RAW || distance < 0) {
            motorController.set(0);
            motorController.stopMotor();
        }
        if (distance > MAX_DISTANCE_RAW + 10 || distance < MAX_DISTANCE_RAW - 20) {
            motorController.set(0);
            motorController.stopMotor();
            motorController.disable();
        }
         */
    }

    // input specific angle to set arm to
    public void targetPosition(double angle) {
        // direction is negative if distance is greater, meaning that the arm has to move down
        // direction is positive if distance is less, meaning arm has to move up
        double direction = (distance > angle) ? -1 : 1;
        // simple math to hopefully have arm just move slower the closer it gets to the target
        double power = 1 - Math.abs(distance - angle) / 300;
        setSpeed(power);
    }

    public void setSpeed(double speed) {

        // constraints to not break stuff (hopefully)
        /*
        if ((distance > MAX_DISTANCE && speed > 0) || (distance < MIN_DISTANCE && speed < 0)) {
            return;
        }

         */
        motorController.set(speed * MAX_POWER);
        SmartDashboard.putNumber("Current set arm speed", speed);
    }

    public void setVoltage(double volts) {
        motorController.setVoltage(MAX_POWER * volts);
    }

    public void stopMotor() {
        motorController.set(0);
        SmartDashboard.putNumber("Current set arm speed", 0);
    }

    public void zeroEncoder() {
        if (encoder != null)
            encoder.setPosition(0);

    }

    // sets the encoder to 90*3.2=288 degrees, WHICH MEANS ARM IS FACING STRAIGHT UP
    public void manualEncoderReset() {
        if (encoder != null)
            encoder.setPosition(288);
    }
}
