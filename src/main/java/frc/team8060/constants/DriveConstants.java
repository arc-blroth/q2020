package frc.team8060.constants;

import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;

public class DriveConstants {

    public static final int JOYSTICK_PORT = 0;

    public static final int XBOXCONTROLLER_PORT = 0; //Edgar: replace this later.
    public static final int LEFT_MOTOR1_PORT = 3;
    public static final int LEFT_MOTOR2_PORT = 2;
    public static final int RIGHT_MOTOR1_PORT = 1;
    public static final int RIGHT_MOTOR2_PORT = 4;
    public static final int[] LEFT_ENCODER_PORTS = {0, 1};
    public static final boolean LEFT_ENCODER_REVERSED = false;

    public static final int[] RIGHT_ENCODER_PORTS = {2, 3};
    public static final boolean RIGHT_ENCODER_REVERSED = true;

    // TODO : complete calibrations needed to get these constant's values

    public static final double ENCODER_DISTANCE_PER_PULSE = 1;

    public static final double S_VOLTS = 0.2;
    public static final double V_VOLT_SECONDS_PER_METER = 1.9;
    public static final double A_VOLT_SECONDS_SQUARED_PER_METER = 0.2;

    public static final double PDRIVE_VEL = 6;

    // distance between tires
    public static final double TRACK_WIDTH_METERS = 0.57785;
    public static final DifferentialDriveKinematics DRIVE_KINEMATICS = new DifferentialDriveKinematics(TRACK_WIDTH_METERS);

}
