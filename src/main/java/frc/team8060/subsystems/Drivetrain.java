package frc.team8060.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.team8060.constants.DriveConstants;

public class Drivetrain extends SubsystemBase {

    private final MotorControllerGroup leftMotors = new MotorControllerGroup(
            new CANSparkMax(DriveConstants.LEFT_MOTOR1_PORT, CANSparkMaxLowLevel.MotorType.kBrushed),
            new CANSparkMax(DriveConstants.LEFT_MOTOR2_PORT, CANSparkMaxLowLevel.MotorType.kBrushed)
    );

    private final MotorControllerGroup rightMotors = new MotorControllerGroup(
            new CANSparkMax(DriveConstants.RIGHT_MOTOR1_PORT, CANSparkMaxLowLevel.MotorType.kBrushed),
            new CANSparkMax(DriveConstants.RIGHT_MOTOR2_PORT, CANSparkMaxLowLevel.MotorType.kBrushed)
    );

    private final DifferentialDrive differentialDrive = new DifferentialDrive(leftMotors, rightMotors);

    private final Encoder leftEncoder = new Encoder(
            DriveConstants.LEFT_ENCODER_PORTS[0],
            DriveConstants.LEFT_ENCODER_PORTS[1],
            DriveConstants.LEFT_ENCODER_REVERSED
    );

    private final Encoder rightEncoder = new Encoder(
            DriveConstants.RIGHT_ENCODER_PORTS[0],
            DriveConstants.RIGHT_ENCODER_PORTS[1],
            DriveConstants.RIGHT_ENCODER_REVERSED
    );

    private final Gyro gyro = new ADXRS450_Gyro();

    private final DifferentialDriveOdometry odometry;

    public Drivetrain() {
        // differentialDrive.setSafetyEnabled(true);
        // differentialDrive.stopMotor();
        // differentialDrive.feed();
        rightMotors.setInverted(true);

        // TODO : setDistance per pulse to correct values after calculating them
        leftEncoder.setDistancePerPulse(DriveConstants.ENCODER_DISTANCE_PER_PULSE);
        rightEncoder.setDistancePerPulse(DriveConstants.ENCODER_DISTANCE_PER_PULSE);
        rightEncoder.

        resetEncoders();
        odometry = new DifferentialDriveOdometry(
                gyro.getRotation2d(),
                leftEncoder.getDistance(),
                rightEncoder.getDistance()
        );
    }

    @Override
    public void periodic() {
        odometry.update(
                gyro.getRotation2d(),
                leftEncoder.getDistance(),
                rightEncoder.getDistance()
        );
        SmartDashboard.putNumber("gyroRotation", gyro.getRotation2d().getDegrees());
        SmartDashboard.putNumber("leftEncoderDistance", leftEncoder.getDistance());
        SmartDashboard.putNumber("rightEncoderDistance", rightEncoder.getDistance());
        SmartDashboard.putNumber("leftEncoderRate", leftEncoder.getRate());
        SmartDashboard.putNumber("rightEncoderRate", rightEncoder.getRate());
        Pose2d pose = getPose();
        SmartDashboard.putNumber("robotPositionX", pose.getX());
        SmartDashboard.putNumber("robotPositionY", pose.getY());
    }

    // returns the currently estimated pose of the robot
    // pose is pretty much its position
    public Pose2d getPose() {
        return odometry.getPoseMeters();
    }

    public void resetOdometry(Pose2d pose) {
        resetEncoders();
        odometry.resetPosition(
                gyro.getRotation2d(),
                leftEncoder.getDistance(),
                rightEncoder.getDistance(),
                pose
        );
    }

    public void zeroOdometry() {
        resetEncoders();
        odometry.resetPosition(
                gyro.getRotation2d(),
                leftEncoder.getDistance(),
                rightEncoder.getDistance(),
                new Pose2d(0, 0, new Rotation2d(0))
        );
    }

    public void resetEncoders() {
        leftEncoder.reset();
        rightEncoder.reset();
    }

    public Encoder getLeftEncoder() {
        return leftEncoder;
    }

    public Encoder getRightEncoder() {
        return rightEncoder;
    }

    public void setMaxOutput(double maxOutput) {
        differentialDrive.setMaxOutput(maxOutput);
    }

    // warning this WILL overshoot
    public void moveDistance(double targetCm) {
        double wheelRadiusCm = 7.62;
        double rotations = targetCm * 2 * wheelRadiusCm * Math.PI;
        double startRotation = leftEncoder.getDistance();
        while (leftEncoder.getDistance() < startRotation + rotations) {
            setSpeed(-0.6, -0.6);
        }
        setSpeed(0, 0);
    }

    public void arcadeDrive(double fwd, double rot) {
        differentialDrive.arcadeDrive(fwd, rot);
    }

    public void tankDriveVolts(double leftVolts, double rightVolts) {
        leftMotors.setVoltage(leftVolts);
        rightMotors.setVoltage(rightVolts);
        differentialDrive.feed();
    }

    public void setSpeed(double leftSpeed, double rightSpeed) {
        differentialDrive.tankDrive(leftSpeed, rightSpeed);
        differentialDrive.feed();
    }

    public DifferentialDriveWheelSpeeds getWheelSpeeds() {
        return new DifferentialDriveWheelSpeeds(leftEncoder.getRate(), rightEncoder.getRate());
    }

    public void zeroHeading() {
        gyro.reset();
    }

    public double getHeading() {
        return gyro.getRotation2d().getDegrees();
    }

    public double getTurnRate() {
        // not sure why this is negative but ok
        return -gyro.getRate(); // degrees per second
    }

}
