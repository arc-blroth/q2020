package frc.team8060.commands;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.team8060.subsystems.ArmControl;

public class ArmControlCommand extends CommandBase {

    private final ArmControl armControl;

    private double power = 1;

    public ArmControlCommand(ArmControl armControl, boolean reversed) {
        if (reversed) {
            power = -1;
        }
        this.armControl = armControl;
        addRequirements(this.armControl);
    }

    @Override
    public void execute() {
        SmartDashboard.putString("Arm Status", "Running");
        armControl.setSpeed(power);
    }

    @Override
    public void end(boolean interrupted) {
        SmartDashboard.putString("Arm Status", "Stopped");
        armControl.stopMotor();
    }

}
