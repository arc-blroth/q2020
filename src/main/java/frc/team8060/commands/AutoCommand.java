package frc.team8060.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.team8060.subsystems.Drivetrain;

import java.time.LocalTime;

public class AutoCommand extends CommandBase {
    private final Drivetrain drivetrain;
    private long startTime;
    private boolean done = false;

    public AutoCommand(Drivetrain drivetrain) {
        this.drivetrain=drivetrain;
        addRequirements(drivetrain);
    }

    @Override
    public void execute() {
        startTime = System.currentTimeMillis();
        while (!done) {
            drivetrain.setSpeed(-0.6, -0.6);
            if (System.currentTimeMillis() - startTime > 4500) {
                done = true;
                drivetrain.tankDriveVolts(0, 0);
            }
        }
        // drivetrain.moveDistance(280);
    }

    @Override
    public void end (boolean interrupted) {
        drivetrain.tankDriveVolts(0, 0);
    }

}
