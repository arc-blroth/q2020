package frc.team8060.commands;

import edu.wpi.first.networktables.DoubleArraySubscriber;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;

public class TestAutoCommand extends CommandBase {

    private final NetworkTable table;

    public TestAutoCommand() {
        table = NetworkTableInstance.getDefault().getTable("visionTable");
    }

    @Override
    public void execute() {
        for (int i = 0; i < 16; i++) {
            DoubleArraySubscriber arr = table.getDoubleArrayTopic("tag" + i).subscribe(new double[] {});
            if (arr.get().length == 4) { // there should only be 4 values - x, y, z, rot
                SmartDashboard.putNumberArray("tag" + i, arr.get());
            }
        }
    }
}
